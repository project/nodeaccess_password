CONTENTS OF THIS FILE
----------------------

  * Introduction
  * Installation
  * Configuration


INTRODUCTION
------------
Maintainer: Daniel Braksator (http://drupal.org/user/134005)

Instructions on http://drupal.org/project/nodeaccess_password.


INSTALLATION
------------
1. Copy nodeaccess_password folder to modules directory.
2. At admin/build/modules enable the Node Access Password module.
3. Enable permissions at admin/user/permissions.
4. Configure the module at admin/user/nodeaccess_password.


CONFIGURATION
-------------
If you want to generate a password to access a node, create a new realm in the config.  
You must give it a name, choose which node types it applies to, and which users will be checked for that password.

Now the idea is that someone in a high user role with the user permission 'view node access passwords' will see the 
passwords on the node, and be able to give the passwords to people who can sign up to the website and enter the 
password into their profile to gain access to the node.

To find out what the password for a node is you could print it out in the node template:

if (user_access('view node access passwords')) {
  print "<div id='nodeaccess-password'>";
  foreach ($node->nodeaccess_password as $realm => $password) {
    print "<div class='nodeaccess-password'><span class='realm'>" . $realm . ":</span> " . $password . "</div>";
  }
  print "</div>";
}

Except you should probably do it in that wacky syntax that templates use.

To find out which realm has granted a user access to a node, then you could do something like this:

global $user;
$my_realms = array();
foreach ($node->nodeaccess_password as $realm => $password) {
  if (strpos($user->nodeaccess_password, $password) !== FALSE) {
    $my_realm[] = $realm;
  }
}
//  all the realms are now stored in $my_realm