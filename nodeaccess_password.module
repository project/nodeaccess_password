<?php

/**
 * Implementation of hook_menu().
 */
function nodeaccess_password_menu() {
  $items['admin/user/nodeaccess_password'] = array(
    'title' => 'Node access password',
    'description' => 'Change default settings for the Node Access Password module.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('nodeaccess_password_admin_form'),
    'access arguments' => array('administer node access passwords')
  );
  return $items;
}

/**
 * Implementation of hook_perm().
 */
function nodeaccess_password_perm() {
  return array('administer node access passwords', 'view node access passwords');
}

/**
 * Menu callback. Draws the admin page.
 */
function nodeaccess_password_admin_form(&$form_state) {
  $max_realm = 1;
  $realm_num = 1;
	while (variable_get('nodeaccess_password_realm_'.$max_realm.'_name', '') != '') {
		$max_realm++;
	}
  $form = array();
  $priority = variable_get('nodeaccess_password_priority', 0);
  $form['nodeaccess_password_priority'] = array(
    '#type' => 'textfield',
    '#title' => t('Priority over other node access modules'),
    '#size' => 3,
    '#maxlength' => 9,
    '#default_value' => is_numeric($priority) ? $priority : 0,
    '#weight' => -2000,
    '#description' => t('If Node Access Password fails to override grants from other node access modules, try setting higher numerical values here.'),
  );
  while ($realm_num <= $max_realm) {
    $lapse = ($realm_num == $max_realm) ? TRUE : FALSE;
    $realm_name = variable_get('nodeaccess_password_realm_'.$realm_num.'_name', '');
    $form['nodeaccess_password_realm_'.$realm_num] = array(
      '#type' => 'fieldset',
      '#attributes' => array('class' => 'nodeaccess-password-realm-'.$realm_num),
      '#title' => $realm_name ? $realm_name : t('New realm'),
      '#weight' => ($realm_num-100),
      '#collapsible' => TRUE,
      '#collapsed' => $lapse,
      '#description' => t(''),
    );
    $form['nodeaccess_password_realm_'.$realm_num]['nodeaccess_password_realm_'.$realm_num.'_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Realm name<span class="form-required" title="This field is required.">*</span>'),
      '#size' => 30,
      '#weight' => 0,
      '#default_value' => variable_get('nodeaccess_password_realm_'.$realm_num.'_name', ''),
      '#description' => t(' NOTE: Only alphanumeric characters and underscores.'),
    );
    $form['nodeaccess_password_realm_'.$realm_num]['nodeaccess_password_realm_'.$realm_num.'_types'] = array(
      '#type' => 'select',
      '#title' => t('Node types'),
      '#default_value' => variable_get('nodeaccess_password_realm_'.$realm_num.'_types', ''),
      '#options' => nodeaccess_password_get_types(),
      '#multiple' => true,
      '#size' => 6,
      '#weight' => 3,
      '#description' => t('Which nodes will be affected by this realm.'),
    );
    $form['nodeaccess_password_realm_'.$realm_num]['nodeaccess_password_realm_'.$realm_num.'_roles'] = array(
      '#type' => 'select',
      '#title' => t('User roles'),
      '#default_value' => variable_get('nodeaccess_password_realm_'.$realm_num.'_roles', ''),
      '#options' => nodeaccess_password_get_roles(),
      '#multiple' => true,
      '#size' => 6,
      '#weight' => 6,
      '#description' => t('Which users will be able to add the password into their profile.  Remember, new registrants will still be anonymous.'),
    );
    $realm_num++;
  }
  return system_settings_form($form);
}

/**
 * Implementation of hook_node_grants().
 */
function nodeaccess_password_node_grants($account, $op) {
  $grants = array();
  if ($account->nodeaccess_password) {
    $user_roles = array_keys($account->roles);
    for ($i = 1; variable_get('nodeaccess_password_realm_'.$i.'_name', '') != ''; $i++) {
      $realm_name = variable_get('nodeaccess_password_realm_'.$i.'_name', '');
      $realm_types = variable_get('nodeaccess_password_realm_'.$i.'_types', '');
      $realm_roles = variable_get('nodeaccess_password_realm_'.$i.'_roles', '');
      foreach ($user_roles as $user_role) {
        if (in_array($user_role, $realm_roles)) {
          $result = db_query("SELECT nid FROM {nodeaccess_password} WHERE INSTR('%s', password) AND realm = '%s'", $account->nodeaccess_password, $realm_name);
          while ($row = db_fetch_array($result)) {
              $grants[$realm_name][] = $row['nid'];
          }
          break;
        }
      }
    }
  }
  return $grants;
}

/**
 * Implementation of hook_user().
 */
function nodeaccess_password_user($op, &$edit, &$account, $category = NULL) {
  if ($op == "form" || $op == "register") {
    $form = array();
    $form['nodeaccess_password'] = array(
      '#type' => 'textarea',
      '#title' => t("Node access passwords"),
      '#default_value' => $account->nodeaccess_password ? $account->nodeaccess_password : '',
      '#description' => t('The title and description here are translatable.'),
      '#rows' => 1,
    );
    return $form;
  }
}

/**
 * Implementation of hook_nodeapi().
 */
function nodeaccess_password_nodeapi(&$node, $op) {
  switch ($op) {
    case 'load':
      $node->nodeaccess_password = array();
      $result = db_query("SELECT realm, password FROM {nodeaccess_password} WHERE nid = %d", $node->nid);
      while ($row = db_fetch_array($result)) {
        $node->nodeaccess_password[$row['realm']] = $row['password'];
      }
    break;
    case 'insert':
    case 'update':
      $grants = array();
      for ($i = 1; variable_get('nodeaccess_password_realm_'.$i.'_name', '') != ''; $i++) {
        $realm_name = variable_get('nodeaccess_password_realm_'.$i.'_name', '');
        $realm_types = variable_get('nodeaccess_password_realm_'.$i.'_types', '');
        $realm_roles = variable_get('nodeaccess_password_realm_'.$i.'_roles', '');
        if (in_array($node->type, $realm_types)) {
          $count = db_result(db_query("SELECT COUNT(*) FROM {nodeaccess_password} WHERE nid = %d AND realm = '%s'", $node->nid, $realm_name));
          if (!$count) {
            db_query("INSERT INTO {nodeaccess_password} (nid, realm, password) VALUES (%d, '%s', '%s')", $node->nid, $realm_name, strtolower(user_password(8)));
          } 
          $grant['gid'] = $node->nid;
          $grant['realm'] = $realm_name;
          $grant['grant_view'] = TRUE;
          $grant['grant_update'] = FALSE;
          $grant['grant_delete'] = FALSE;
          $grants[] = $grant;
        }
        $grants[] = $grant;
      }
    break;
    case 'delete':
      // Deleting node, delete related permissions.
      db_query('DELETE FROM {nodeaccess_password} WHERE nid = %d', $node->nid);
    break;
  }
}

/**
 * Implementation of hook_node_access_records().
 */
function nodeaccess_password_node_access_records($node) {
  if (nodeaccess_password_disabling()) {
    return;
  }
  $priority = variable_get('nodeaccess_password_priority', 0);
  $grants = array();
  for ($i = 1; variable_get('nodeaccess_password_realm_'.$i.'_name', '') != ''; $i++) {
    $realm_name = variable_get('nodeaccess_password_realm_'.$i.'_name', '');
    $realm_types = variable_get('nodeaccess_password_realm_'.$i.'_types', '');
    $realm_roles = variable_get('nodeaccess_password_realm_'.$i.'_roles', '');
    if (in_array($node->type, $realm_types)) {
      $grants[] = array(
        'realm' => $realm_name,
        'gid' => $node->nid,
        'grant_view' => TRUE,
        'grant_update' => FALSE,
        'grant_delete' => FALSE,
        'priority' => $priority,
      );
    }
  }
  return $grants;
}

/**
 * Implementation of hook_enable().
 */
function nodeaccess_password_enable() {
}

/**
 * Implementation of hook_disable().
 */
function nodeaccess_password_disable() {
  nodeaccess_password_disabling(TRUE);
}

function nodeaccess_password_disabling($set = NULL) {
  static $disabling = FALSE;
  if ($set !== NULL) {
    $disabling = $set;
  }
  return $disabling;
}

/**
 * Get an array of content types for use in select forms
 */
function nodeaccess_password_get_types() {
  $types = node_get_types($op = 'types', $node = NULL, $reset = FALSE);
  $type_array = array();
  foreach ((array)$types as $type) {
    $type_array[$type->type] = $type->name;
  }
  if (count($type_array) == 1) {
    return false;
  }
  return $type_array;
}

/**
 * Get an array of user roles for use in select forms
 */

function nodeaccess_password_get_roles() {
  $role_array = array();
  $role_array = /*$role_array +*/ user_roles();
  if (count($role_array) == 1) {
    return false;
  }
  return $role_array;
}